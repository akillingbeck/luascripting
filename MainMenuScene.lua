local storyboard = require( "storyboard" )
local scene = storyboard.newScene()
local gameData = require "CustomData"
local widget = require "widget"
local easingx  = require("easingx")

print("AYE")
--storyboard.isDebug = true
----------------------------------------------------------------------------------
-- 
--      NOTE:
--      
--      Code outside of listener functions (below) will only be executed once,
--      unless storyboard.removeScene() is called.
-- 
---------------------------------------------------------------------------------
local previousActiveButton = nil
local finalX = nil
local finalY =  nil

local startX = nil
local startY =  nil
local buttonSize = nil
local snd_click = audio.loadSound("tongueClick.ogg")
local snd_pop = audio.loadSound("pop.ogg")




local function addTweenCanceller( button )
    function button:cancelTween( obj )
        if obj ~= nil then
            transition.cancel(obj)
            obj = nil
        end
    end
end

local function handleModeSelect( event )
    local phase = event.phase
    local button = event.target
    if "began" == phase then
        audio.play(snd_click)

        if previousActiveButton ~= button then
            previousActiveButton:DeActivate()
            previousActiveButton.infoText:Hide(700)

            button:Activate()
            button.infoText:Show()

            previousActiveButton = button
        else
            button:Activate()
            button.infoText:Show()

            previousActiveButton = button
        end
    end
end 

local function StartSelectedGameMode( event )
     local phase = event.phase
     local button = event.target
     local buttonParent = event.target.parent
     local gameType = previousActiveButton
     local evX = event.x
     local evY = event.y
     local btnX = buttonParent.x
     local btnY = buttonParent.y

     local inBounds = (evX > btnX - (button.width*0.5) and evX < btnX + (button.width*0.5)) and (evY > btnY - (button.height*0.5) and evY < btnY + (button.height*0.5))

     if phase == "began" then      
        print("began phase")
       
         button:setFillColor(0,65,72)
        display.getCurrentStage():setFocus(button)

    elseif phase == "moved" then
        if inBounds == false then
           button:setFillColor(0,109,100)
        else
            button:setFillColor(0,65,72)
        end

    elseif phase == "ended" or phase == "cancelled" then

            if inBounds== true then
                audio.play(snd_pop)
                print("GAMETYPE "..gameType.name)
                local sceneToLoad = ""
                 if gameType.name == "Normal" then
                   sceneToLoad = "NormalGameMode"
                elseif "Gravity" == gameType.name then
                    sceneToLoad = "GravityGameMode"
                elseif "Copycat" ==gameType.name then
                     sceneToLoad = "BobbleGameMode"
                end

                  local options = 
                    {
                      effect = "crossFade",
                      time = 500,

                      params = 
                      {
                        GameData=gameData,
                        prevScene = "MainMenuScene",
                        LoadScene=sceneToLoad
                      }
                    }

                    
                    storyboard.gotoScene("LoadingScene",options)
                   
                    
                end


        button:setFillColor(0,109,100)
        display.getCurrentStage():setFocus(nil)
    end

         return true
end

--Adds a show, Hide and TweenCanceller methods to the object
local function addShowHideFunctions(displayObject )

        addTweenCanceller(displayObject)

     function displayObject:Hide(delay)
        local timeDelay = 0
        if delay ~= nil then
            timeDelay = delay
        end
       displayObject:cancelTween(displayObject.tween)
       displayObject.tween = transition.to(displayObject,{y=displayObject.HidY,alpha=0,transition=easing.outExpo,time=timeDelay,onComplete=function ()
          displayObject:cancelTween(displayObject.tween)
       end})
    end

      function displayObject:Show()
        displayObject.y = displayObject.HidY
        displayObject.x =  displayObject.VisX

        displayObject:cancelTween(displayObject.tween)
       displayObject.tween = transition.to(displayObject,{y=displayObject.VisY,alpha=1, time=500,transition=easingx.easeOutBack,onComplete=function ()
          displayObject:cancelTween(displayObject.tween)
       end})
    end
end

local function addActivateFunctions( button )
    function button:Activate(delay)
         button.tween = transition.to(button,{alpha=1,time=100,transition=easing.outExpo,onComplete=function ()
          button:cancelTween(button.tween)
       end})
    end
     function button:DeActivate(delay)
        local timeDelay = 0
        if delay ~= nil then
            timeDelay = delay
        end
         button.tween = transition.to(button,{alpha=0.5,time=timeDelay,transition=easing.outExpo,onComplete=function ()
          button:cancelTween(button.tween)
       end})
    end
end


local function GetBestTime(gameMode)
    local BestTime = nil
    if gameMode == "Normal" then
        BestTime = gameData.SaveData.NModeEasyTime
    elseif gameMode == "Gravity" then
        BestTime = gameData.SaveData.GModeEasyTime
    elseif gameMode == "Copycat" then
        BestTime = gameData.SaveData.BModeEasyTime
    end

    if BestTime > 0 then
        local seconds =  math.floor((BestTime / 1000))
      local  minutes = math.floor((seconds / 60))
         seconds = seconds % 60
       local  hundreds = BestTime % 1000

        return display.newText(string.format("%02d:%02d:%02d",minutes,seconds,hundreds), 0, 0, native.systemFont, 22)
    else
        return display.newText(string.format("--:--:--",0,0), 0, 0, native.systemFont, 32)
    end


        
end

local function createGameModeButton(parentGroup,buttonXPos,buttonYPos,frameIndex,GameMode,imageSheet)

    
    local button = widget.newButton
        {
            left = display.contentCenterX,
            top = display.contentCenterY,
            width = 200,
            height = 50,
            fontSize = 32,
            sheet = imageSheet,
            defaultFrame = frameIndex,
            overFrame = frameIndex,
            id = ("button_"..frameIndex),
            onEvent = handleModeSelect,
        }
        button:setReferencePoint(display.CenterReferencePoint)
        button.name = GameMode
        button.x = buttonXPos--display.contentCenterX - buttonWidth
        button.y = buttonYPos--display.contentCenterY
        button.width = buttonSize
        button.height = buttonSize
  

        local dGroup = display.newGroup()
        local modeText = display.newText(GameMode, 0, 0,native.systemFontBold, 32)
        modeText:setReferencePoint(display.CenterReferencePoint)
        modeText.x = 0
        modeText.y = -button.height*0.5
        modeText:setTextColor(0,0,0)
        local highScore = GetBestTime(GameMode)
        highScore:setTextColor(0,0,0)
        highScore.x = 0
        highScore.y = 0
        dGroup:insert(modeText)
        dGroup:insert(highScore)
        dGroup.VisX = finalX
        dGroup.VisY = finalY
        dGroup.HidX = startX
        dGroup.HidY = startY
        dGroup.x = finalX
        dGroup.y = finalY

        addShowHideFunctions(dGroup)

        button.infoText = dGroup --Add the text group with highscore etc as a variable to the button, May need to remove this reference?

        parentGroup:insert(button)
        parentGroup:insert(dGroup)

        addTweenCanceller(button)
        addActivateFunctions(button)

        return button
end 
---------------------------------------------------------------------------------
-- BEGINNING OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------
local function removeTween(obj)
 if obj.tween ~= nil then
    transition.cancel(obj.tween)
    obj.tween = nil
 end
end
-- Called when the scene's view does not exist:
function scene:createScene( event )

        print("RECREATING")
        local sheetInfo = gameData.SheetInfo
        local imageSheet = gameData.GraphicsSheet
        buttonSize = display.contentWidth * 0.25
       

        finalX =  display.contentCenterX --Final X position of display group for game mode ( with high score) (Goes to this position when Show() called)
        finalY =   display.contentHeight * 0.45 --Final Y position of display group for game mode ( with high score)

        startX =  -display.contentWidth --Final X position of display group for game mode ( with high score) (Goes to this X position when Hide() called)
        startY = display.contentHeight * 0.25-- -display.contentHeight --Start Y position of display group for game mode ( with high score) (This is set directly before the Show is called)



        local group = self.view
        local width = display.contentWidth*1.5
        local height = display.contentHeight*1.5
        local background = display.newRect(0,0,width,height)
        background:setReferencePoint(display.CenterReferencePoint)
        background.x = display.contentWidth*0.5 
        background.y =  display.contentHeight*0.5 
        background:setFillColor(238,238,238)

         group:insert(background)
        --Create a menu button
        --Parent , buttonX, buttonY, frameIndex, ModeType, imageSheet
        local normalModeButton = createGameModeButton(group,display.contentCenterX - buttonSize,display.contentHeight - buttonSize*3.0,sheetInfo:getFrameIndex("NormalMode"),"Normal",imageSheet)
        local gravityModeButton = createGameModeButton(group,display.contentCenterX,display.contentHeight - buttonSize*2.0,sheetInfo:getFrameIndex("GravityMode"),"Gravity",imageSheet)
        local bobbleModeButton = createGameModeButton(group,display.contentCenterX+ buttonSize,display.contentHeight - buttonSize*3.0 ,sheetInfo:getFrameIndex("BobbleMode"),"Copycat",imageSheet)

        normalModeButton.x = -display.contentWidth*2.0
        gravityModeButton.y = display.contentHeight*2.0
        bobbleModeButton.x = display.contentWidth*2.0

        removeTween(normalModeButton)
        normalModeButton.tween = transition.to(normalModeButton,{x=display.contentCenterX - buttonSize,time=200,delay=500,transition=easing.outExpo,onComplete=function ()
            normalModeButton:Activate()
           
             removeTween(normalModeButton)
        end})

         removeTween(gravityModeButton)
        gravityModeButton.tween = transition.to(gravityModeButton,{y=display.contentHeight - buttonSize*2.0,time=200,delay=600,transition=easing.outExpo,onComplete=function ()
             removeTween(gravityModeButton)
        end})

         removeTween(bobbleModeButton)
        bobbleModeButton.tween = transition.to(bobbleModeButton,{x=display.contentCenterX+ buttonSize,time=200,delay=700,transition=easing.outExpo,onComplete=function ()
             removeTween(bobbleModeButton)
        end})

       -- normalModeButton:DeActivate()
        normalModeButton.infoText:Hide()
        gravityModeButton:DeActivate()
        gravityModeButton.infoText:Hide()
        bobbleModeButton:DeActivate()
        bobbleModeButton.infoText:Hide()

      
        previousActiveButton = normalModeButton

        local frameIndex = sheetInfo:getFrameIndex("Button")
        local startGroup = display.newGroup()
        local StartGame = display.newRoundedRect(0, 0, display.contentWidth*0.5, (display.contentWidth*0.5)*0.285, 30)
        local StartText = display.newText("Play Now", 0, 0, native.systemFont, 22)
        StartText:setReferencePoint(display.CenterReferencePoint)
        StartText.x = 0
        StartText.y = 0


        StartGame:addEventListener("touch",StartSelectedGameMode)

        StartGame:setFillColor(0,109,100)
        StartGame:setReferencePoint(display.CenterReferencePoint)
        StartGame.x =0
        
        StartGame.width = display.contentWidth*0.5
        StartGame.height = (display.contentWidth*0.5)*0.285
        StartGame.y =0

       

        startGroup:insert(StartGame)
        startGroup:insert(StartText)
        startGroup.x = display.contentCenterX
        startGroup.y =display.contentHeight*2.0 

         removeTween(startGroup)
        startGroup.tween =  transition.to(startGroup,{y=display.contentHeight - StartGame.height,time=200,delay=800,transition=easing.outExpo,onComplete=function ()
             removeTween(startGroup)
              normalModeButton.infoText:Show()

             end})

        group:insert(startGroup)
      
        -----------------------------------------------------------------------------

        --      CREATE display objects and add them to 'group' here.
        --      Example use-case: Restore 'group' from previously saved state.

        -----------------------------------------------------------------------------

end


-- Called BEFORE scene has moved onscreen:
function scene:willEnterScene( event )
        local group = self.view
        
       
        -----------------------------------------------------------------------------

        --      This event requires build 2012.782 or later.

        -----------------------------------------------------------------------------

end


-- Called immediately after scene has moved onscreen:
function scene:enterScene( event )
        local group = self.view


        storyboard.removeAll()
         if event.params ~= nil then
       -- storyboard.removeScene(event.params.prevScene)
        end
        -----------------------------------------------------------------------------

        --      INSERT code here (e.g. start timers, load audio, start listeners, etc.)

        -----------------------------------------------------------------------------

end


-- Called when scene is about to move offscreen:
function scene:exitScene( event )
        local group = self.view

        -----------------------------------------------------------------------------

        --      INSERT code here (e.g. stop timers, remove listeners, unload sounds, etc.)

        -----------------------------------------------------------------------------

end


-- Called AFTER scene has finished moving offscreen:
function scene:didExitScene( event )
        local group = self.view

        -----------------------------------------------------------------------------

        --      This event requires build 2012.782 or later.

        -----------------------------------------------------------------------------

end


-- Called prior to the removal of scene's "view" (display group)
function scene:destroyScene( event )
        audio.dispose( snd_click )
        snd_click = nil

        audio.dispose( snd_pop )
        snd_pop = nil
        local group = self.view
        print("DESTROY MENU")

        previousActiveButton.infoText:removeSelf()
        previousActiveButton.infoText = nil
        previousActiveButton:removeSelf()
        previousActiveButton = nil
        gameData = nil
        widget = nil
        easingx = nil

        finalY=nil
        finalX = nil
        startX = nil
        startY =  nil
        buttonSize = nil


        --Runtime:removeEventListener("enterFrame",storyboard.printMemUsage)
        -----------------------------------------------------------------------------

        --      INSERT code here (e.g. remove listeners, widgets, save state, etc.)

        -----------------------------------------------------------------------------

end


-- Called if/when overlay scene is displayed via storyboard.showOverlay()
function scene:overlayBegan( event )
        local group = self.view
        local overlay_name = event.sceneName  -- name of the overlay scene

        -----------------------------------------------------------------------------

        --      This event requires build 2012.797 or later.

        -----------------------------------------------------------------------------

end


-- Called if/when overlay scene is hidden/removed via storyboard.hideOverlay()
function scene:overlayEnded( event )
        local group = self.view
        local overlay_name = event.sceneName  -- name of the overlay scene

        -----------------------------------------------------------------------------

        --      This event requires build 2012.797 or later.

        -----------------------------------------------------------------------------

end

--Runtime:addEventListener( "enterFrame", storyboard.printMemUsage )


---------------------------------------------------------------------------------
-- END OF YOUR IMPLEMENTATION
---------------------------------------------------------------------------------

-- "createScene" event is dispatched if scene's view does not exist
scene:addEventListener( "createScene", scene )

-- "willEnterScene" event is dispatched before scene transition begins
scene:addEventListener( "willEnterScene", scene )

-- "enterScene" event is dispatched whenever scene transition has finished
scene:addEventListener( "enterScene", scene )

-- "exitScene" event is dispatched before next scene's transition begins
scene:addEventListener( "exitScene", scene )

-- "didExitScene" event is dispatched after scene has finished transitioning out
scene:addEventListener( "didExitScene", scene )

-- "destroyScene" event is dispatched before view is unloaded, which can be
-- automatically unloaded in low memory situations, or explicitly via a call to
-- storyboard.purgeScene() or storyboard.removeScene().
scene:addEventListener( "destroyScene", scene )

-- "overlayBegan" event is dispatched when an overlay scene is shown
scene:addEventListener( "overlayBegan", scene )

-- "overlayEnded" event is dispatched when an overlay scene is hidden/removed
scene:addEventListener( "overlayEnded", scene )

---------------------------------------------------------------------------------

return scene